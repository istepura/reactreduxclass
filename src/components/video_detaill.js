import React from "react";

const VideoDetail = ({ video: { id, snippet } }) => {
  const videoUrl = `https://www.youtube.com/embed/${id.videoId}`;

  return (
    <div className="video-detail col-md-8">
      <div className="embed-responsive embed-responsive-16by9">
        <iframe className="embed-responsive-item" src={videoUrl} />
      </div>
      <div className="details">
        <div>{snippet.title}</div>
        <div>{snippet.descrition}</div>
      </div>
    </div>
  );
};

export default VideoDetail;
