import _ from "lodash";
import React from "react";
import ReactDOM from "react-dom";
import keys from "./config/key";
import SearchBar from "./components/SearchBar";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detaill";
import YTSearch from "youtube-api-search";

const Loading = () => {
  return <div>Loading...</div>;
};
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { videos: [], selected: null };
    this.videoSearch("fender acoustic");
  }

  render() {
    return (
      <div>
        <SearchBar
          onSearchTermChange={_.debounce(term => {
            this.videoSearch(term);
          }, 300)}
        />
        {this.state.selected ? (
          <VideoDetail video={this.state.selected} />
        ) : (
          <Loading />
        )}
        <VideoList
          videos={this.state.videos}
          onVideoSelect={selected => this.setState({ selected })}
        />
      </div>
    );
  }

  videoSearch(term) {
    YTSearch({ key: keys.API_KEY, term }, videos =>
      this.setState({ videos: videos, selected: videos[0] })
    );
  }
}

ReactDOM.render(<App />, document.querySelector(".container"));
